
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:28PM

See merge request itentialopensource/adapters/adapter-nexus_repository!11

---

## 0.4.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-nexus_repository!9

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:41PM

See merge request itentialopensource/adapters/adapter-nexus_repository!8

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:52PM

See merge request itentialopensource/adapters/adapter-nexus_repository!7

---

## 0.4.0 [05-09-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/devops-netops/adapter-nexus_repository!6

---

## 0.3.3 [03-26-2024]

* Changes made at 2024.03.26_14:23PM

See merge request itentialopensource/adapters/devops-netops/adapter-nexus_repository!5

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_13:21PM

See merge request itentialopensource/adapters/devops-netops/adapter-nexus_repository!4

---

## 0.3.1 [02-26-2024]

* Changes made at 2024.02.26_13:46PM

See merge request itentialopensource/adapters/devops-netops/adapter-nexus_repository!3

---

## 0.3.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/devops-netops/adapter-nexus_repository!2

---

## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/devops-netops/adapter-nexus_repository!1

---

## 0.1.1 [07-12-2021]

- Initial Commit

See commit abfbdd2

---
