# Sonatype Nexus Repository

Vendor: Sonatype Nexus Repository
Homepage: https://www.sonatype.com/

Product: Sonatype Nexus Repository
Product Page: https://www.sonatype.com/products/sonatype-nexus-repository

## Introduction
We classify Sonatype Nexus Repository into the CI/CD domain as Sonatype Nexus Repository handles artifact management processes within secure, organized repositories.

## Why Integrate
The Sonatype Nexus Repository adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Sonatype Nexus Repository. With this adapter you have the ability to perform operations on items such as:

- Repositories
- Assets

## Additional Product Documentation
The [API documents for Sonatype Nexus Repository](https://help.sonatype.com/en/rest-and-integration-api.html)
