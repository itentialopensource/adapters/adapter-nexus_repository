
## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/devops-netops/adapter-nexus_repository!1

---

## 0.1.1 [07-12-2021]

- Initial Commit

See commit abfbdd2

---
